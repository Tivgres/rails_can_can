require 'active_support/concern'

module Premiumable
  extend ActiveSupport::Concern

  def manage_premium
    unless current_user.has_role?(:paid) || current_user.has_role?(:admin)
      range = 0..((@post.premium.size * 0.15).ceil)
      @post.premium = ("#{@post.premium[range]}...")
    end
  end
end