class Ability
  include CanCan::Ability

  def initialize(user)
    alias_action :create, :read, :update, :destroy, to: :any
    alias_action :create, :read, to: :watch_plus
    alias_action :read, to: :watch

    user ||= User.new

    if user.has_role?(:admin)
      can :any, :all
    elsif user.has_role?(:paid)
      can :watch_plus, :all
    else #user.has_role?(:free)
      can :watch, :all
    end
  end
end
