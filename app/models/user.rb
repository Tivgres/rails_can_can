class User < ApplicationRecord
  rolify
  # rolify :role_cname => 'Premium'
  # rolify :role_cname => 'Free'
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
end
