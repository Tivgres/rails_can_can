# Admin user
User.create(email: 'admin@example.com', password: '44332211')
User.last.add_role(:admin)

# Paid user
User.create(email: 'paid@example.com', password: '44332211')
User.last.add_role(:paid)

# Free user
User.create(email: 'free@example.com', password: '44332211')
# It's really don't needed in current the configurations, but any time we must look to the future
User.last.add_role(:free)
