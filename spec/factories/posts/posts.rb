require 'faker'

FactoryBot.define do
  factory :post do
    factory :post_full, parent: :post do
      title { Faker::Lorem.word }
      body { Faker::Lorem.paragraph }
    end
  end
end
