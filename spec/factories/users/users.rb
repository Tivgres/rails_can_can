require 'faker'

FactoryBot.define do
  factory :user do
    factory :user_full, parent: :user do
      email { Faker::Internet.email }
      password { Faker::Internet.password }
    end
  end
end
