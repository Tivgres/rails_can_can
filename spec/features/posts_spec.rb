require 'rails_helper'

describe 'User login', type: :feature do

  before(:each) do
    user = create(:user_full)
    visit '/users/sign_in'
    fill_in 'user_email', with: user.email
    fill_in 'user_password', with: user.password
    click_button('Log in')
  end

  after(:context) do
    DatabaseCleaner.strategy = :transaction
    DatabaseCleaner.clean_with(:truncation)
  end

  context 'posts with admin', js: true do
    it 'success with admin role to create a post and went to this page' do
      User.last.add_role(:admin)
      visit '/posts'
      click_link('New Post')
      expect(page).to have_current_path(new_post_path)
      fill_in 'post_title', with: 'title'
      fill_in 'post_body', with: 'body'
      click_button('Create Post')
      expect(page).to have_current_path(post_path(Post.last.id))
    end

    it 'success with admin role to create a update and went to this page' do
      User.last.add_role(:admin)
      visit '/posts'
      click_link('Edit')
      expect(page).to have_current_path(edit_post_path(Post.last))
      fill_in 'post_title', with: 'title2'
      fill_in 'post_body', with: 'body2'
      click_button('Update Post')
      expect(page).to have_current_path(post_path(Post.last.id))
      expect(page).to have_content('Post was successfully updated.')
    end

    it 'success with admin role to create a update and went to this page' do
      User.last.add_role(:admin)
      visit '/posts'
      click_link('Destroy')
      page.driver.browser.switch_to.alert.accept
      expect(page).to have_current_path(posts_path)
      expect(page).to have_content('Post was successfully destroyed.')
    end
  end

  context 'posts with paid' do
    it 'success with paid role to create a post and went to this page' do
      User.last.add_role(:paid)
      visit '/posts'
      click_link('New Post')
      expect(page).to have_current_path(new_post_path)
      fill_in 'post_title', with: 'title'
      fill_in 'post_body', with: 'body'
      click_button('Create Post')
      expect(page).to have_current_path(post_path(Post.last.id))
    end
  end

  context 'posts with free' do
    it 'success with free role to go to post page' do
      User.last.add_role(:free)
      visit '/posts'
      click_link('Show')
      expect(page).to have_current_path(post_path(Post.last.id))
    end
  end
end