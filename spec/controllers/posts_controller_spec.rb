require 'rails_helper'

# It's like little bit a messy & DRY
# But really it's was written speciality
# for a better testing when cancan have a lot o difference by roles
RSpec.describe PostsController, type: :controller do
  render_views

  before do
    user = build(:user_full)
    user.save
    allow(controller).to receive(:authenticate_user!).and_return(true)
    allow(controller).to receive(:current_user).and_return(user)
  end

  after do
    DatabaseCleaner.strategy = :transaction
    DatabaseCleaner.clean_with(:truncation)
  end

  describe 'success tests' do
    context 'admin' do
      it 'should be success for new' do
        User.last.add_role 'admin'
        get :new
        expect(response).to have_http_status(200)
        expect(response).to render_template('new')
        expect(assigns(:post)).to be_truthy
      end

      it 'should be success for create' do
        User.last.add_role 'admin'
        post :create, params: { post: FactoryBot.attributes_for(:post_full) }
        expect(response).to have_http_status(302)
        expect(assigns(:post)).to be_truthy
      end

      it 'should be success for show' do
        User.last.add_role 'admin'
        create(:post_full)
        get :show , params: { id: Post.last.id }
        expect(response.status).to eq(200)
        expect(assigns(:post)).to eq(Post.last)
      end

      it 'should be success for edit' do
        User.last.add_role 'admin'
        create(:post_full)
        get :edit , params: { id: Post.last.id }
        expect(response.status).to eq(200)
        expect(assigns(:post)).to eq(Post.last)
      end

      it 'should be success for update' do
        User.last.add_role 'admin'
        create(:post_full)
        title = 'qwe-qwe'
        put :update, params: { id: Post.last.id, post: { title: title } }
        expect(response.status).to eq(302)
        expect(assigns(:post).title).to eq(title)
      end

      it 'should be success for index' do
        User.last.add_role 'admin'
        create(:post_full)
        get :index
        expect(response.status).to eq(200)
        expect(assigns(:posts).size).to eq(1)
      end

      it 'should be success for delete' do
        User.last.add_role 'admin'
        create(:post_full)
        expect {
          delete :destroy, params: { id: Post.last.id }
        }.to change(Post, :count).by(-1)
        expect(response).to redirect_to(posts_path)
      end
    end

    context 'paid' do
      it 'should be success for new' do
        User.last.add_role 'paid'
        get :new
        expect(response).to have_http_status(200)
        expect(response).to render_template('new')
        expect(assigns(:post)).to be_truthy
      end

      it 'should be success for create' do
        User.last.add_role 'paid'
        post :create, params: { post: FactoryBot.attributes_for(:post_full) }
        expect(response).to have_http_status(302)
        expect(assigns(:post)).to be_truthy
      end

      it 'should be success for show' do
        User.last.add_role 'paid'
        create(:post_full)
        get :show , params: { id: Post.last.id }
        expect(response.status).to eq(200)
        expect(assigns(:post)).to eq(Post.last)
      end

      it 'should be success for index' do
        User.last.add_role 'paid'
        create(:post_full)
        get :index
        expect(response.status).to eq(200)
        expect(assigns(:posts).size).to eq(1)
      end

      it 'should be unsuccessful for update' do
        User.last.add_role 'paid'
        create(:post_full)
        put :update, params: { id: Post.last.id, post: { title: 'qwe-qwe' } }
        expect(response.status).to eq(403)
      end

      it 'should be unsuccessful for delete' do
        User.last.add_role 'paid'
        create(:post_full)
        expect {
          delete :destroy, params: { id: Post.last.id }
        }.to change(Post, :count).by(0)
        expect(response.status).to eq(403)
      end
    end

    context 'free' do
      it 'should be unsuccessful for new' do
        User.last.add_role 'free'
        get :new
        expect(response).to have_http_status(403)
      end

      it 'should be unsuccessful for create' do
        User.last.add_role 'free'
        post :create, params: { post: FactoryBot.attributes_for(:post_full) }
        expect(response).to have_http_status(403)
      end

      it 'should be success for show' do
        User.last.add_role 'free'
        create(:post_full)
        get :show , params: { id: Post.last.id }
        expect(response.status).to eq(200)
        expect(assigns(:post)).to eq(Post.last)
      end

      it 'should be unsuccessful for update' do
        User.last.add_role 'free'

      end

      it 'should be success for index' do
        User.last.add_role 'free'
        create(:post_full)
        put :update, params: { id: Post.last.id, post: { title: 'qwe-qwe' } }
        expect(response.status).to eq(403)
      end

      it 'should be unsuccessful for delete' do
        User.last.add_role 'free'
        create(:post_full)
        expect {
          delete :destroy, params: { id: Post.last.id }
        }.to change(Post, :count).by(0)
        expect(response.status).to eq(403)
      end
    end
  end
end
